console.log('start integration');

// Object binding to DOM
const DOMPARSER = new DOMParser().parseFromString.bind(new DOMParser());
const FEEDURL = 'https://www.basketeurope.com/feed/';

// Detection if fetch WEB API is available
if (self.fetch) {
	console.log('fetch API available');
	try {
		// Construct url object
		var url = new URL(FEEDURL);
		console.log(url.href);
		// Instanciate DOM fragment
		var frag = document.createDocumentFragment()
		var hasBegun = true
		// Retrieve xml content
		fetch(url).then((res) => {
			res.text().then((xmlTxt) => {
				try {
					let doc = DOMPARSER(xmlTxt, "text/xml")
					let heading = document.createElement('span')
					heading.textContent = url.hostname
					frag.appendChild(heading)
					// loop on feed items
					doc.querySelectorAll('item').forEach((item) => {
						let i = item.querySelector.bind(item);
						let div = document.createElement('div');
						let a = document.createElement('a');
						let p = document.createElement('p');
						// Fill html element with feed item data
						a.href = i('link').textContent;
						a.textContent = i('title').textContent;
						a.target = '_BLANK';
						div.appendChild(a);
						// Attach html element to DOM fragment
						frag.appendChild(div);
					})
				} catch (e) {
					console.error('Error in parsing the feed')
				}
				if (hasBegun) {
					hasBegun = false;
				}
				// Attach DOM fragment to DOM basketeurope element
				document.querySelector('basketeurope').appendChild(frag)
			})
		}).catch((error) => console.error(error.message))
	} catch (e) {
		console.error('URL invalid');
	}
} else {
	console.log('fetch API unavailable');
}
console.log('end integration');
