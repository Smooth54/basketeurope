﻿console.log('start integration');

// Object binding to DOM
const DOMPARSER = new DOMParser().parseFromString.bind(new DOMParser());
const FEEDURL = 'https://www.basketeurope.com/feed/';

//Load CSS file
var head  = document.getElementsByTagName('head')[0];
var link  = document.createElement('link');
link.rel  = 'stylesheet';
link.type = 'text/css';
link.href = 'https://www.basketeurope.com/widgetbusa/widgetbusa2/style.css';
link.media = 'all';
head.appendChild(link);

// Detection if fetch WEB API is available
if (self.fetch) {
	console.log('fetch API available');
	try {
		// Construct url object
		var url = new URL(FEEDURL);
		console.log(url.href);
		// Instanciate DOM fragment
		var frag = document.createDocumentFragment()
		var frag2 = document.createDocumentFragment()
		var hasBegun = true
		// Add id
		document.getElementsByTagName('basketeurope')[0].id ="flux-BE";
		// Retrieve xml content
		fetch(url).then((res) => {
			res.text().then((xmlTxt) => {
				try {
					let doc = DOMPARSER(xmlTxt, "text/xml")
					let heading = document.createElement('h3')
					heading.textContent = url.hostname
					frag2.appendChild(heading)
					// loop on feed items
					doc.querySelectorAll('item').forEach((item) => {
						let i = item.querySelector.bind(item);
						let div = document.createElement('div');
						let a = document.createElement('a');
						let p = document.createElement('p');
						// Fill html element with feed item data
						a.href = i('link').textContent;
						a.textContent = i('title').textContent;
						a.target = '_BLANK';
						div.appendChild(a);
						// Attach html element to DOM fragment
						frag.appendChild(div);
					})
				} catch (e) {
					console.error('Error in parsing the feed')
				}
				if (hasBegun) {
					hasBegun = false;
				}

				// Create link READMORE
				let linkBE = document.createElement('a')
				linkBE.textContent = "Actualités suivantes"
				linkBE.href = "https://"+url.hostname
				linkBE.target = "_BLANK"
				linkBE.className = "readmore-BE"
				frag.appendChild(linkBE)

				// Create container
				let container = document.createElement('div')
				container.className = "container-flux-BE"
				container.appendChild(frag)

				// Attach DOM fragment to DOM basketeurope element
				let compile = document.querySelector('basketeurope')
				compile.appendChild(frag2)
				compile.appendChild(container)
			})
		}).catch((error) => console.error(error.message))

	} catch (e) {
		console.error('URL invalid');
	}
} else {
	console.log('fetch API unavailable');
}
console.log('end integration');
